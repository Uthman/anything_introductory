#include <stdio.h>
#include <math>

int main()
{
    unsigned int num = 0;
    printf("Enter a number: ");
    scanf("%d", num);
    
    
    for (unsigned int count = 1; count <= 10; ++count)
    {
        printf("%d raised to power %d equals %d\n", num, count, pow(num, count));
    }
    
    return 0;
}
